package Servlets;

import DAOs.Article;
import DAOs.ArticleDAO;
import DAOs.Comment;
import DAOs.CommentDAO;
import SecurityPack.AuthenticationModule;
import com.google.gson.Gson;
import org.owasp.encoder.Encode;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class CommentServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("got todoPost");
        Cookie[] cookies = req.getCookies();

        for (int i = 0; i < cookies.length; i++) {
            String name = cookies[i].getName();
            String value = cookies[i].getValue();

            System.out.println("cookies");

            if (value.equals("delete")) {
//                req.getSession().setAttribute("newComment",req.getParameter("commentTextArea"));
//                this won't work, you have to find the particular area of the comment you want to delete.

                       req.getSession().setAttribute("commentID", Encode.forJava(req.getParameter("commentID")  ) );
               


                        deleteComment(req, resp);
                cookies[i].setMaxAge(0);



            }
            if (value.equals("create")){

                System.out.println("create");

//                We already have the username in the session..
//                We already have the article id in the form


                req.getSession().setAttribute("commentText",Encode.forJava(req.getParameter("text"))  );
                req.getSession().setAttribute("articleID", Encode.forJava(req.getParameter("articleID"))    );


                if (req.getParameterMap().containsKey("rID")){

                    req.getSession().setAttribute("rID", Encode.forJava(req.getParameter("rID")));
                }



// TODO: 29/05/18 delete these cookies after you use them! 

                makeNewComment(req,resp);
                cookies[i].setMaxAge(0);

            }
        }


    }

//    private void displayCommentsList(HttpServletRequest request, HttpServletResponse response) {
//
//        try (CommentDAO toGetList=new CommentDAO()){
//
//            String placeholder=(String)request.getSession().getAttribute("articleID");
//            int articleID=Integer.parseInt(placeholder);
//
//
//            List<Comment> toStoreComments=toGetList.getAllCommentsByArticleId(articleID);
//
//            request.getSession().setAttribute("comments",toStoreComments);
//
//
//
////            request.getRequestDispatcher("/jsp/mainPage.jsp").forward(request, response);
//
//        }catch (Exception e){
//            System.out.println("Exception occured");
//        }
//
//
//    }


    private void makeNewComment(HttpServletRequest req, HttpServletResponse resp){

        System.out.println("make new comment");

        HttpSession session=req.getSession();
        try  (CommentDAO makeNewComment=new CommentDAO()){
            Comment comment=new Comment();

            String newContent= Encode.forJava((String)session.getAttribute("commentText")) ;
            System.out.println(newContent);
            String placeHolder= (String)session.getAttribute("articleID")  ;
            String image=(String) session.getAttribute("image");
            System.out.println(image);


            String refID=(String)session.getAttribute("rID");
            System.out.println(refID);

          if (refID == null){

              int articleID=Integer.parseInt(placeHolder);


              String userName= Encode.forJava ((String)session.getAttribute("username")) ;


              comment.setArticleId(articleID);
              comment.setContent(newContent);
              comment.setUsername(userName);
              comment.setAvatar(image);

              makeNewComment.createNewComment(comment);



          }else{
              int referenceID=Integer.parseInt((String)session.getAttribute("rID"));




              int articleID=Integer.parseInt(placeHolder);
              String userName= Encode.forJava ((String)session.getAttribute("username")) ;
              if (userName==null){
                  userName="guest";
              }



              comment.setReferenceId(referenceID);
              comment.setArticleId(articleID);
              comment.setContent(newContent);
              comment.setUsername(userName);
              comment.setAvatar(image);

              System.out.println("refID "+referenceID+" ArticleID "+articleID+" content "+newContent+" username "+userName);



              makeNewComment.createNestedComment(comment);


          }



//            int articleID=Integer.parseInt(placeHolder);
//
//
//            String userName= Encode.forJava ((String)session.getAttribute("username")) ;
//
//
//            comment.setArticleId(articleID);
//            comment.setContent(newContent);
//            comment.setUsername(userName);



            //makeNewComment.createNewComment(comment);
        }catch (Exception e){
            System.out.println("Exception occured hered");
            System.out.println(e.getMessage());
        }
    }



    private void deleteComment(HttpServletRequest request,HttpServletResponse response){

        //this method creates an ArticleDAO and from there, calls the delete article method to remove it from the database
        System.out.println("I got to deleteComment!");

        String commentID=(String) request.getSession().getAttribute("commentID");
        int id=Integer.parseInt(commentID);

        System.out.println(""+id);
        try (CommentDAO toControl=new CommentDAO()){

         Comment toCheck=toControl.getCommentById(id);

            AuthenticationModule toAuthenticate = new AuthenticationModule();
             boolean isSafeToDelete = toAuthenticate.check(request.getSession(), toCheck);

            if (isSafeToDelete){

                toControl.deleteComment(id);
                System.out.println("I deleted!");
                request.getRequestDispatcher("articles.jsp").forward(request,response);

            }else {

                request.getRequestDispatcher("/jsp/ERROR/noPermission.jsp").forward(request, response);

            }



        } catch (Exception e) {
            System.out.println("Exception occured here2");
            System.out.println(e.getMessage());
        }


    }
}
